# Django settings for kedaia project.

import os
import mongoengine

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

# for gmail 
EMAIL_HOST = "smtp.gmail.com"
EMAIL_HOST_USER = "kontak@keudee.com"
# oigxqvsclqtbzccd for firdauskoder@gmail.com
EMAIL_HOST_PASSWORD = "fbezxqeicvrfhdkk"
EMAIL_PORT = 587
EMAIL_USE_TLS = True

DISABLED_STRING_AUTHOR = ["/", ",", ".", "`"]

MANAGERS = ADMINS

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
SESSION_ENGINE = 'mongoengine.django.sessions'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy'
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ID'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # os.path.join(BASE_DIR, "static"),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '4r1d))b*8prml*nsej47kl2h2-$au&%udfjq7zq#$pm#wqa3k6'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    # 'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

SITE_ID = ""

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.static',
    )

ROOT_URLCONF = 'kedaia.urls'
SUBDOMAIN_URLCONFS = {
    
}


# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'kedaia.wsgi.application'

TEMPLATE_DIRS = (
     os.path.join(os.path.dirname(__file__), '../templates'),
)


COMPRESS_ENABLED = True

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'portal',
    'compressor',
    'subdomains',
    # Uncomment the next line to enable the admin:
    # 'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)



SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


WEBSITES =  {
    'social_media' : {
        'facebook' : 'https://www.facebook.com/Keudee-Aceh-401440216719461/',
        'twitter' : 'https://twitter.com/keudee',
        'instagram' : 'https://Instagram.com',
        'pinterest' : 'https://Pinterest.com',
        'flickr' : 'https://flickr.com'
    },
    'contact_person' : {
        'email' : 'contact@keudee.com',
        'phone' : '+62-852-7054-6820'
    },
    'meta_information' : {
        'description' : "Keudee merupakan pasar online terbesar di Aceh, Setiap individu dapat berjualan dan mengelola toko online mereka secara mudah dan gratis. Jual beli jadi lebih menyenangkan. Punya toko online?",
        'title' : "Keudee - Temukan Apapun di Aceh",
        'keywords' : "Temukan Apapun di Keudee",
        'author' : "firdaus, fird0s, Muhammad Firdaus",
        'location' : 'Lampaseh Kota, Banda Aceh',
        'copyright' : 'Copyright 2015, Keudee.com'
    },
    'upload' : {
        'product' : MEDIA_ROOT + "/products/",
        'product_thumbnails' : MEDIA_ROOT + "/products/thumbnails/",
        'photo_profile' : MEDIA_ROOT + "/profile/",
        'blog' : MEDIA_ROOT + "/admin/blog/",
        'slider' : MEDIA_ROOT + "/admin/slider/",
        'header_author' : MEDIA_ROOT + "/header_author/",
        'purchase' : MEDIA_ROOT + '/purchase/',
    },
    'rating' : {
        'popular' : 100,
        'hot' : 340
    },
    'online_plugin' : {
        'google_analytics' : """(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                                ga('create', 'UA-69272814-1', 'auto');
                                ga('send', 'pageview');""",
        'facebook_website' : """window.fbAsyncInit = function() {
            FB.init({
              appId      : '514632702050782',
              xfbml      : true,
              version    : 'v2.5'
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/id_ID/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));"""
    }
}

#Mongo Setting 
mongoengine.connect("kedaia")

#for local settings 
try:
    from local_settings import *
except ImportError:
    pass