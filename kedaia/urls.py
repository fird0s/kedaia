from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView
from portal import views
from django.conf.urls.static import static
from django.conf import settings

from portal  import views
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^beranda/', views.beranda, name='beranda'),
	url(r'^thumbnails/', views.index_thumbnails, name='index_thumbnails'),
	url(r'^login/', views.login, name='login'),
	url(r'^search/', views.search, name='search'),
	url(r'^register/', views.register, name='register'),
	url(r'^forgot_password/', views.forgot_password, name='forgot_password'),
	url(r'^log_out/$', views.log_out, name='log_out'),
	url(r'^account/profile/$', views.profile, name='profile'),
	url(r'^account/social_media/$', views.social_media, name='social_media'),
	url(r'^account/add_product/$', views.add_product, name='add_product'),
	url(r'^account/change_password/$', views.change_password, name='change_password'),
	url(r'^account/products/$', views.products, name='products'),
	url(r'^account/product/edit/(?P<id>[a-zA-Z0-9]+)', views.edit_product, name='edit_product'),
	url(r'^account/product/delete/(?P<id>[a-zA-Z0-9]+)', views.delete_product, name='delete_product'),

	url(r'^account/feedback/$', views.account_feedback, name='account_feedback'),
	url(r'^account/settings/$', views.account_settings, name='account_settings'),


	# Account Transaction
	url(r'^konfirmasi/$', views.confirmation, name='confirmation'),
	url(r'^konfirmasi/(?P<id>[a-zA-Z0-9]+)$', views.confirmation_info_purchase, name='confirmation_with_purchase'),
	
	url(r'^account/transaksi/$', views.account_transaction, name='account_transaction'),
	url(r'^account/transaksi/(?P<id>[a-zA-Z0-9]+)$', views.account_detail_transaction, name='account_detail_transaction'),
	url(r'^account/transaksi/delete/(?P<id>[a-zA-Z0-9]+)$', views.account_delete_transaction, name='account_delete_transaction'),

	# Email Confirmation
	url(r'^email_confirmation/$', views.email_confirmation, name='email_confirmation'),	
	url(r'^email_confirmation/verification/(?P<id>[a-zA-Z0-9_-]+)/$', views.email_verification, name='email_verification'),	


	# PURCHASE
	url(r'^shopping_cart/$', views.shopping_cart, name='shopping_cart'),
	url(r'^shopping_cart/buy/(?P<id>[a-zA-Z0-9]+)$', views.shopping_cart_confirmation, name='shopping_cart_confirmation'),

	url(r'^author/(?P<slug_url>[a-zA-Z0-9_-]+)/$', views.author, name='author'),
	
	# AUTHOR
	url(r'^author/$', views.shopping_cart, name='shopping_cart'),

	url(r'^p/(?P<slug>[a-zA-Z0-9_-]+)', views.display_product, name='display_product'),
	url(r'^p-id/(?P<id>[a-zA-Z0-9_-]+)', views.display_product_by_id, name='display_product_by_id'),
	

	url(r'^ajax/sub_category/$', views.ajax_sub_category, name='ajax_sub_category'),
	url(r'^category/(?P<slug_url>[a-zA-Z0-9_-]+)/$', views.category, name='category'),
	url(r'^category_thumbnails/(?P<slug_url>[a-zA-Z0-9_-]+)', views.category_thumbnails, name='category_thumbnails'),
	

	url(r'^blogs/', views.blogs, name='blogs'),
	url(r'^blog/(?P<slug>[a-zA-Z0-9_-]+)', views.display_blog, name='display_blog'),
	

	url(r'^page/(?P<page>[.a-zA-Z0-9_-]+)', views.page, name='page'),


	# admin 
	url(r'^administrator/$', views.administrator, name='administrator'),	
	url(r'^administrator/login/$', views.admin_login, name='admin_login'),


	# admin category
	url(r'^administrator/categories/$', views.admin_categories, name='admin_categories'),	
	url(r'^administrator/category/edit/(?P<id>[a-zA-Z0-9]+)', views.admin_edit_category, name='admin_edit_category'),
	url(r'^administrator/category/create/$', views.admin_create_category, name='admin_create_category'),

	# admin pages
	url(r'^administrator/pages/$', views.admin_pages, name='admin_pages'),	
	url(r'^administrator/page/edit/(?P<id>[a-zA-Z0-9_-]+)', views.admin_edit_page, name='admin_edit_page'),
	url(r'^administrator/page/delete/(?P<id>[a-zA-Z0-9_-]+)', views.admin_delete_page, name='admin_delete_page'),
	url(r'^administrator/page/create/$', views.admin_create_page, name='admin_create_page'),

	# admin users
	url(r'^administrator/users/$', views.admin_users, name='admin_users'),
	url(r'^administrator/user/edit/(?P<id>[a-zA-Z0-9]+)', views.admin_edit_user, name='admin_edit_user'),

	# admin regions
	url(r'^administrator/regions/$', views.admin_regions, name='admin_regions'),
	url(r'^administrator/region/edit/(?P<id>[a-zA-Z0-9]+)', views.admin_edit_region, name='admin_edit_region'),

	# admin search
	url(r'^administrator/search/$', views.admin_search, name='admin_search'),
	url(r'^administrator/search/delete/(?P<id>[a-zA-Z0-9]+)', views.admin_delete_search, name='admin_delete_search'),

	# admin sliders 
	url(r'^administrator/sliders/$', views.admin_slider, name='admin_slider'),
	url(r'^administrator/slider/edit/(?P<id>[a-zA-Z0-9]+)', views.admin_edit_slider, name='admin_edit_slider'),
	url(r'^administrator/slider/delete/(?P<id>[a-zA-Z0-9]+)', views.admin_delete_slider, name='admin_delete_slider'),
	url(r'^administrator/slider/create/$', views.admin_create_slider, name='admin_create_slider'),

	# admin blog
	url(r'^administrator/blogs/$', views.admin_blogs, name='admin_blogs'),
	url(r'^administrator/blog/create/$', views.admin_create_blog, name='admin_create_blog'),
	url(r'^administrator/blog/edit/(?P<id>[a-zA-Z0-9]+)', views.admin_edit_blog, name='admin_edit_blog'),
	url(r'^administrator/blog/delete/(?P<id>[a-zA-Z0-9]+)', views.admin_delete_blog, name='admin_delete_blog'),

	# admin products
	url(r'^administrator/products/$', views.admin_products, name='admin_products'),
	url(r'^administrator/product/delete/(?P<id>[a-zA-Z0-9]+)', views.admin_delete_product, name='admin_delete_product'),
	url(r'^administrator/product/edit/(?P<id>[a-zA-Z0-9]+)', views.admin_edit_product, name='admin_edit_product'),

	# admin list province and shipping cost 
	url(r'^administrator/list_province/$', views.admin_shipping_cost, name='admin_shipping_cost'),
	url(r'^administrator/list_province/edit/(?P<id>[a-zA-Z0-9]+)', views.admin_edit_shipping, name='admin_edit_shipping'),
	# admin login

	# test
	url(r'^test/$', views.test, name='test'),	


)

handler404 = 'portal.views.error_404'
handler500 = 'portal.views.error_500'

if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
        (r'media/(?P<path>.*)', 'serve', {'document_root': settings.MEDIA_ROOT}),
    )

if settings.DEBUG:
    urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)