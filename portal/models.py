from mongoengine import *
from django.db import models
import datetime

class ShippingCost(Document):
    province = StringField()
    city_and_districts = DictField()


class ListProvince(Document):
    province = StringField(max_length=100, unique=True)
    date_add = DateTimeField(default=datetime.datetime.now())
    shipping_cost = StringField(max_length=100)
    latitude = StringField()
    longtitude = StringField()

class ListRegion(Document):
    region = StringField(max_length=100, unique=True)
    date_add = DateTimeField(default=datetime.datetime.now())
    latitude = StringField()
    longtitude = StringField()

class Users(Document):
    email = StringField(unique=True, required=True)
    full_name = StringField(max_length=50)
    last_login = DateTimeField(default=datetime.datetime.now())
    date_joined = DateTimeField(default=datetime.datetime.now())
    password = StringField()
    is_active = BooleanField(default=True)
    is_validated = BooleanField(default=False) #email verified
    
    def __repr__(self):
        return "email: %s, password: %s" % (self.email, self.password)

class Kedai(Document):
    author = ReferenceField(Users)
    store_name = StringField(max_length=50, unique=True)
    address = StringField(max_length=200)
    region = ReferenceField(ListRegion)
    postal_code = StringField(max_length=12)
    phone_number = StringField(max_length=20)
    about_store = StringField(max_length=2000)
    logo_image = StringField()
    header_image = StringField()
    page_viewer = IntField(default=100)
    store_url_slug = StringField(max_length=30, unique=True)
    auto_maketing = BooleanField(default=True)
    pengiriman_pembayaran = StringField()
    bank_account = StringField(default="BCA")
    bank_no_account = StringField()
    map_latitude = StringField()
    map_longtitude = StringField()


class SocialMedia(Document):
    kedai = ReferenceField(Kedai)
    facebook = StringField(max_length=100, default="http://facebook.com")
    twitter = StringField(max_length=100, default="http://twitter.com")
    google_plus = StringField(max_length=100, default="https://plus.google.com")
    instagram = StringField(max_length=100, default="https://instagram.com")
    pinterest = StringField(max_length=100, default="https://pinterest.com")
    BBM = StringField(max_length=20, default="0000000")
    date_add = DateTimeField(default=datetime.datetime.now())

class Product_Category(Document):
    category_name = StringField(max_length=30, unique=True)
    css_class_name = StringField(max_length=30, unique=True)
    slug_url = StringField(max_length=100, unique=True)
    date_add = DateTimeField(default=datetime.datetime.now())
    meta = {'collection': 'product_category'}

class Sub_Product_Category(Document):
	category_name = StringField()
	date_add = DateTimeField(default=datetime.datetime.now())
	category_index = ReferenceField(Product_Category)
        css_class_name = StringField(max_length=30)


class Products(Document):
    kedai = ReferenceField(Kedai)
    product_name = StringField(max_length=200)
    price = StringField(max_length=50)
    status_publish = BooleanField(default=True)
    condition = BooleanField(default=True) #true if new and false if second
    information = StringField(max_length=2000)
    shipping_and_payment = StringField(max_length=2000)
    quantity = IntField(default=1)
    images = ListField()
    date_add = DateTimeField(default=datetime.datetime.now())
    last_update = DateTimeField(default=datetime.datetime.now())
    category = ReferenceField(Product_Category)
    sub_category = ReferenceField(Sub_Product_Category)
    is_active = BooleanField(default=True)
    slug_url = StringField(max_length=300)
    viewer = IntField(default=1)
    weight = IntField(default=1)
    tags = ListField()


#-------ADMIN--------#
class AdminPage(Document):
    page_name = StringField(max_length=200)
    content = StringField()
    slug_url = StringField(max_length=300, unique=True)
    date_created = DateTimeField(default=datetime.datetime.now())
    page_viewer = IntField(default=1)

#-------ADMIN--------#
class AdminSearch(Document):
    search = StringField()
    date_searched = DateTimeField(default=datetime.datetime.now())
    viewer = IntField(default=1)
    meta_info = StringField()

#-------ADMIN--------#
class AdminSlider(Document):
    slider_name = StringField(max_length=1000)
    content = StringField()
    image = StringField()
    date_created = DateTimeField(default=datetime.datetime.now())
    redirect_url = StringField()

class AdminBlog(Document):
    post_name = StringField(max_length=1000)
    content = StringField()
    date_created = DateTimeField(default=datetime.datetime.now())
    viewer = IntField(default=1)
    slug_url = StringField(max_length=1000)
    image_thumbnail = StringField(max_length=200)
    tags = ListField()

class AdminUser(Document):
    full_name = StringField()
    email = StringField(unique=True)
    phone_number = StringField()
    password = StringField()
    date_joined = DateTimeField(default=datetime.datetime.now)

class Purchase(Document):
    kedai = ReferenceField(Kedai)
    product = ReferenceField(Products)
    name_buyer = StringField()
    email_buyer = StringField()
    hp_buyer = StringField()
    to_name_buyer = StringField()
    to_hp_buyer = StringField()
    to_province = ReferenceField(ListProvince)
    to_region = StringField()
    basic_price = StringField()
    quantity = IntField()
    time_purchase = DateTimeField(default=datetime.datetime.now)
    time_payment = DateTimeField()
    status_payment = BooleanField(False)
    status = StringField(default="Belum ada bukti pembayaran.")
    address = StringField()
    read = BooleanField(False)
    date_order = DateTimeField(default=datetime.datetime.now())

    # total semua produk, quantity * harga satuan
    def satuan_product(self):
        total = int(self.basic_price) / self.quantity
        return total

    def total_payment(self):
        return int(self.basic_price) + int(self.to_province.shipping_cost)

class payment_confirmation(Document):
    buyer_name = StringField()
    code_payment = ReferenceField(Purchase) 
    amount_of_tranfer = StringField() # Jumlah Pembayaran
    name_saving_book = StringField() # Nama sesuai buku tabungan
    evidence_payment = StringField() # File bukti pembayaran
    date_payment = DateTimeField(default=datetime.datetime.now())

