from django.shortcuts import render, redirect
from django.conf import settings
from django.conf.urls import handler404, handler500
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail, EmailMultiAlternatives
from portal.models import *

# Create your views here.
import re, os
import random, string
from slugify import slugify
from PIL import Image
from library import *
import smtplib
import string
import locale 

def confirmation_info_purchase(request, id):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None
	blogs = AdminBlog.objects.order_by("-id").limit(4)			
	WEBSITES = settings.WEBSITES
	try:
		purchase = Purchase.objects.get(id=id)
	except DoesNotExist:
		messages.add_message(request, messages.ERROR, 'Mohon Maaf, Kode Pembayaran Anda tidak valid, Silahkan Coba Lagi.')	
		return redirect("confirmation")
	except ValidationError:
		return redirect("confirmation")

	payment = None	
	try:	
		payment = payment_confirmation.objects.get(code_payment=purchase)
	except DoesNotExist:
		pass		

	if request.method == "POST":
		if request.POST.get("buyer_name") and request.POST.get("code_payment") and request.POST.get("total_transfer") and request.POST.get("name_passbook") and request.FILES.get("bukti_transfer"):
			confirmation = payment_confirmation()
			confirmation.buyer_name = request.POST.get("buyer_name")


			try:
				confirmation.code_payment = Purchase.objects.get(id=request.POST.get("code_payment"))
			except DoesNotExist:
				messages.add_message(request, messages.ERROR, 'Mohon Maaf, Kode Pembayaran Anda tidak valid.')	
			except ValidationError:
				messages.add_message(request, messages.ERROR, 'Mohon Maaf, Kode Pembayaran Anda tidak valid.')	
			confirmation.amount_of_tranfer = request.POST.get("total_transfer")
			confirmation.name_saving_book = request.POST.get("name_passbook")

			if request.FILES.get("bukti_transfer"):	
				bukti_transfer = request.FILES.get("bukti_transfer")
				bukti_transfer.name = random_generator()

				dest = open(settings.WEBSITES['upload']['purchase']+bukti_transfer.name, "wb+")

				for chunk in bukti_transfer.chunks():
					dest.write(chunk)
				dest.close()

				confirmation.evidence_payment = bukti_transfer.name
				confirmation.save()

				# ganti status purchase
				try: 
					change_purchase = Purchase.objects.get(id=request.POST.get("code_payment"))
					change_purchase.status = "Sudah Mengirim Bukti Pembayaran"
					change_purchase.save()
				except ValidationError:
					messages.add_message(request, messages.ERROR, 'Mohon Maaf, Kode Pembayaran Anda tidak valid.')	


				messages.add_message(request, messages.SUCCESS, 'Terima Kasih Telah Mengirim Bukti Pembayaran, Kami Akan Menghubungi Anda Secapat Mungkin.')	
				return redirect("/konfirmasi/%s" % change_purchase.id)
	return render(request, "purchase/confirmation_info_purchase.html", {'kedai' : kedai, 'WEBSITES' : WEBSITES, 'purchase' : purchase, 'blogs' : blogs, 'payment' : payment})

def confirmation(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None
	blogs = AdminBlog.objects.order_by("-id").limit(4)		
	WEBSITES = settings.WEBSITES
	if request.method == "POST":
		if request.POST.get("buyer_name") and request.POST.get("code_payment") and request.POST.get("total_transfer") and request.POST.get("name_passbook") and request.FILES.get("bukti_transfer"):
			confirmation = payment_confirmation()
			confirmation.buyer_name = request.POST.get("buyer_name")
			try:
				confirmation.code_payment = Purchase.objects.get(id=request.POST.get("code_payment"))
			except DoesNotExist:
				messages.add_message(request, messages.ERROR, 'Mohon Maaf, Kode Pembayaran Anda tidak valid.')	
			except ValidationError:
				messages.add_message(request, messages.ERROR, 'Mohon Maaf, Kode Pembayaran Anda tidak valid.')	
			confirmation.amount_of_tranfer = request.POST.get("total_transfer")
			confirmation.name_saving_book = request.POST.get("name_passbook")

			if request.FILES.get("bukti_transfer"):
				bukti_transfer = request.FILES.get("bukti_transfer")
				bukti_transfer.name = random_generator()

				dest = open(settings.WEBSITES['upload']['purchase']+bukti_transfer.name, "wb+")

				for chunk in bukti_transfer.chunks():
					dest.write(chunk)
				dest.close()

				confirmation.evidence_payment = bukti_transfer.name
				confirmation.save()

				# ganti status purchase
				try: 
					change_purchase = Purchase.objects.get(id=request.POST.get("code_payment"))
					change_purchase.status = "Sudah Mengirim Bukti Pembayaran"
					change_purchase.save()
				except ValidationError:
					messages.add_message(request, messages.ERROR, 'Mohon Maaf, Kode Pembayaran Anda tidak valid.')	



				messages.add_message(request, messages.SUCCESS, 'Terima Kasih Telah Mengirim Bukti Pembayaran, Kami Akan Menghubungi Anda Secapat Mungkin.')	
				return redirect("confirmation")

	return render(request, "purchase/confirmation.html", {'WEBSITES' : WEBSITES, 'kedai' : kedai, 'blogs' : blogs})

def test(request):
	return render(request, "test.html")

def send_email_purchase(to, id_product):
	try:
		purchase = Purchase.objects.get(id=id_product)
	except DoesNotExist:
		pass
	subject, to, password = "Silahkan Lakukan Pembayaran", to, settings.EMAIL_HOST_PASSWORD
	text_content = "Hello, %s. Silahkan lakukan pembayaran pada produk %s (@ Rp. %s ) %s item = %s, dan biaya pengiriman Rp. %s. Total Keseluruhan Rp. %s" \
	% (purchase.name_buyer, purchase.product.product_name, purchase.satuan_product, purchase.quantity, purchase.basic_price, purchase.to_province.shipping_cost, purchase.total_payment())
	html_content = """
			<html>
			  <head></head>	
			  <body style="font-family:Helvetica,arial,sans-serif;color:#666666;line-height:20px;">
			    <p>Hello, <span style="font-weight: bold;">{0}</span>.</p>
			    <p>Silahkan Lakukan Pembayaran pada Pemesanan Barang Anda sbb:</p>
			    <p>Kode Pembayaran : {1}<p>
			    <p>Produk <b>{2}</b> (@ Rp. {3:n}) {4} item = Rp. {5:n}.</p>
			    <p>Biaya Pengiriman = Rp. {6:n}.</p>
			    <p><b>Total Keseluruhan = Rp. {7:n}</b></p>
			    <p>Dikirim ke <b>Bank {8}</b>, No. Rekening <b>{9}</b>.</p>
			    <p>Untuk melakukan konfirmasi pembayaran, silahkan kunjungi <b><a href="http://keudee.com{10}">link ini</a></b>.</p>
			    <br>
			    <p>Terima Kasih.<br>
			    Tim Keudee.com</p>
			  </body>
			</html>
			""".format(purchase.name_buyer, purchase.id, purchase.product.product_name, purchase.satuan_product(), purchase.quantity, int(purchase.basic_price), int(purchase.to_province.shipping_cost), purchase.total_payment(), purchase.product.kedai.bank_account, purchase.product.kedai.bank_no_account, reverse('confirmation'))
	msg = EmailMultiAlternatives(subject, text_content, settings.EMAIL_HOST_USER, [to], headers={'Message-ID': 'foo'})
	msg.attach_alternative(html_content, "text/html")
	msg.send()		

def send_email_forgotpass(to, full_name, password):
	subject, to, password = "Lupa Password di Keudee.com", to, password
	text_content = "Hello, %s. Password Anda adalah %s, Silahkan login di http://keudee.com/login/" % (full_name, password)
	html_content = """
			<html>
			  <head></head>	
			  <body style="font-family:Helvetica,arial,sans-serif;color:#666666;line-height:20px;">
			    <p>Hello, <span style="font-weight: bold;">{0}</span>.</p>
			    <p>Password Anda adalah <b>{1}</b>, Silahkan login <a target="_blank" href="http://keudee.com/login/">disini</p>
			    <br>
			    <p>Terima Kasih.<br>
			    Tim Keudee.com</p>
			  </body>
			</html>
			""".format(full_name, password)
	msg = EmailMultiAlternatives(subject, text_content, settings.EMAIL_HOST_USER, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()


def forgot_password(request):
	WEBSITES = settings.WEBSITES
	if request.method == "POST":
		if request.POST.get("email"):
			try:
				get_email = Users.objects.get(email=request.POST.get("email"))
				messages.add_message(request, messages.SUCCESS, 'Silahkan Buka Email Anda, Kami Sudah Mengirim Passwordnya.')	
				send_email_forgotpass(to=get_email.email, full_name=get_email.full_name, password=get_email.password)
			except DoesNotExist:
				messages.add_message(request, messages.ERROR, 'Maaf Anda Tidak Terdaftar Sebagai Member.')			
	return render(request, "forgot_password.html", {'WEBSITES' : WEBSITES})

def email_verification(request, id):
	try:
		get_email = Users.objects.get(id=id)
		get_email.is_validated = True
		get_email.save()
		messages.add_message(request, messages.SUCCESS, 'Email Anda Sudah Terverifikasi, Silahkan Login.')	
		return redirect("login")
	except DoesNotExist:
		messages.add_message(request, messages.ERROR, 'Email Anda Belum Terdaftar.')
		return redirect("register")	

def send_email_verification(from_email, to, full_name, link):
	subject, from_email, to = "Verifikasi Email Keudee.com", from_email, to
	text_content = "Hello, %s. Untuk melakukan verifikasi silahkan kunjungi lini ini %s" % (full_name, link)
	html_content = """
			<html>
			  <head></head>
			  		
			  <body style="font-family:Helvetica,arial,sans-serif;color:#666666;line-height:20px;">
			    <p>Hello, <span style="font-weight: bold;">{0}</span>.</p>
			    <p>Untuk melakukan verifikasi silahkan kunjungi <a href='{1}'>link ini</a>.</p>
			    <br>
			    <p>Terima Kasih.<br>
			    Tim Keudee.com</p>

			  </body>
			</html>
			""".format(full_name, link)
	msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
	msg.attach_alternative(html_content, "text/html")
	msg.send()

def email_confirmation(request):
	WEBSITES = settings.WEBSITES
	if request.method == "POST":
		try:
			get_email = Users.objects.get(email=request.POST.get("email"))

			# send email for verification
			link = "http://keudee.com"+reverse('email_verification', kwargs={'id':get_email.id})
			send_email_verification(from_email=settings.EMAIL_HOST_USER, to=get_email.email, full_name=get_email.full_name, link=link)

			messages.add_message(request, messages.SUCCESS, 'Silahkan Buka Email Anda Untuk Verifikasi.')	
			return redirect("login")

		except DoesNotExist:
			messages.add_message(request, messages.ERROR, 'Email Anda Belum Terdaftar.')	
	return render(request, "email_confirmation.html", {'WEBSITES' : WEBSITES})

def error_404(request):
	return render(request, "error_page/404.html", status=404)

def error_500(request):
	return render(request, "error_page/404.html")

def author(request, slug_url):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	try:	
		author = Kedai.objects.get(store_url_slug=slug_url)
	except DoesNotExist:
		return redirect("index")

	blogs = AdminBlog.objects.order_by("-id").limit(4)
	paginator = Paginator(Products.objects(kedai=author).order_by("-id"), 8)
	page = request.GET.get('page')


	# sort for condition, 0 is for both of new and second. 1 is new and 2 is second product.
	condition = ""
	if request.GET.get("condition") == "0":
		condition = request.GET.get("condition")
		paginator = Paginator(Products.objects(kedai=author).order_by("-id"), 8)

	if request.GET.get("condition") == "1":
		condition = request.GET.get("condition")
		paginator = Paginator(Products.objects(kedai=author, condition=True).order_by("-id"), 8)

	if request.GET.get("condition") == "2":
		condition = request.GET.get("condition")
		paginator = Paginator(Products.objects(kedai=author, condition=False).order_by("-id"), 8)	

	# sort_product for sorting product
	# 1 == newest product
	# 2 == product name (sort by product name like alphabet)
	# 3 == most viewed
	# 4 == lowest price
	# 5 == highest price
	sort_product = ""
	if request.GET.get("sort_product") == "1":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=author).order_by("-id"), 8)

	if request.GET.get("sort_product") == "2":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=author).order_by("product_name"), 8)	

	if request.GET.get("sort_product") == "3":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=author).order_by("-viewer"), 8)			

	if request.GET.get("sort_product") == "4":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=author).order_by("price"), 8)				

	if request.GET.get("sort_product") == "5":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=author).order_by("-price"), 8)						

	# search, it useful for people who wants to find a product.
	search_product = ""
	if request.GET.get("q"):
		search_product = request.GET.get("q")
		paginator = Paginator(Products.objects(product_name__icontains=request.GET.get('q'), kedai=author).order_by("-price"), 8)						

	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)

	popular_products = Products.objects.order_by("-viewer").limit(6)
	WEBSITES = settings.WEBSITES
	return render(request, "author/author.html", {'products' : products, 'WEBSITES' : WEBSITES, 'popular_products' : popular_products, 'author' : author, 'paginator' : paginator, 'condition' : condition, 'sort_product' : sort_product, 'search_product' : search_product, 'blogs' : blogs, 'kedai' : kedai })

def shopping_cart_confirmation(request, id):

	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None
	list_region = ListRegion.objects.order_by("-id")
	try:
		product = Products.objects.get(id=id)
	except DoesNotExist:
		return redirect("index")

	list_province = ListProvince.objects.order_by("province")	

	if request.method=="POST":
		if request.POST.get("buyer_name") and request.POST.get("email_buyer") \
		and request.POST.get("hp_buyer") and request.POST.get("buyer_name_reciever") and request.POST.get("buyer_reciever_hp1") \
		and request.POST.get("buyer_reciever_hp2") and request.POST.get("quantity") and request.POST.get("price") \
		and request.POST.get("to_region") and request.POST.get("address"):
			
			# save to purchase database
			buyer = Purchase()
			if kedai:
				buyer.kedai = Kedai.objects.get(id=product.kedai.id)

			buyer.name_buyer = request.POST.get("buyer_name") 	
			buyer.email_buyer = request.POST.get("email_buyer") 	
			buyer.hp_buyer = request.POST.get("hp_buyer") 	
			buyer.to_name_buyer = request.POST.get("buyer_name_reciever") 	
			buyer.to_email_buyer = request.POST.get("buyer_reciever_hp1") 	

			buyer.product = product
			buyer.basic_price = request.POST.get("price").replace(".", "")
			buyer.quantity = request.POST.get("quantity")
			buyer.from_region = ListRegion.objects.get(id=product.kedai.region.id)
			buyer.to_region = request.POST.get("to_region")
			buyer.to_province = ListProvince.objects.get(id=request.POST.get("to_province"))
			buyer.address = request.POST.get("address")
			buyer.save()

			locale.setlocale(locale.LC_ALL, '')
			send_email_purchase(to=buyer.email_buyer, id_product=buyer.id)
			return redirect("/konfirmasi/%s" % buyer.id)


	WEBSITES = settings.WEBSITES	
	blogs = AdminBlog.objects.order_by("-id").limit(4)	
	return render(request, "purchase/edit-shopping-cart.html", {'list_region' : list_region, 'product' : product, 'WEBSITES' : WEBSITES, 'kedai' : kedai, 'blogs' : blogs, 'list_province' : list_province})

def shopping_cart(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None
	WEBSITES = settings.WEBSITES
	blogs = AdminBlog.objects.order_by("-id").limit(4)	
	return render(request, "purchase/shopping-cart.html", {'kedai' : kedai, 'WEBSITES' : WEBSITES, 'blogs' : blogs})

def index(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None
	product_category = Product_Category.objects.order_by('category_name')	
	products = Products.objects.order_by("-id").limit(12)
	popular_products = Products.objects.order_by("-viewer").limit(5)
	slider = AdminSlider.objects.order_by("-id").limit(5)
	WEBSITES = settings.WEBSITES
	blogs = AdminBlog.objects.order_by("-id").limit(4)
	return render(request, "index/index/index.html", {'kedai' : kedai, 'product_category' : product_category, 'WEBSITES' : WEBSITES, 'products' : products, 'popular_products' : popular_products, 'blogs' : blogs, 'slider' : slider})

def account_settings(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None
	blogs = AdminBlog.objects.order_by("-id").limit(4)	
	list_region = ListRegion.objects.order_by("-id")
	WEBSITES = settings.WEBSITES
	if request.method == "POST":
		if request.POST.get("region") and request.POST.get("latitude") and request.POST.get("longtitude"):
			kedai.region =  ListRegion.objects.get(id=request.POST.get("region"))
			kedai.map_latitude = request.POST.get("latitude")
			kedai.map_longtitude = request.POST.get("longtitude")
			kedai.bank_account = request.POST.get("bank_account")
			kedai.bank_no_account = request.POST.get("no_rekening")
			kedai.pengiriman_pembayaran = request.POST.get("pengiriman_pembayaran")
			kedai.save()

		if request.FILES.get("header_image"):
			header_image = request.FILES.get("header_image")	
			header_image.name = random_generator() + ".jpg"

			dest = open(settings.WEBSITES['upload']['header_author']+header_image.name, "wb+")

			for chunk in header_image.chunks():
				dest.write(chunk)
			dest.close()

			try:
				compress_change_ext(settings.WEBSITES['upload']['header_author']+header_image.name, target=settings.WEBSITES['upload']['header_author']+header_image.name, quality=30)
			except IOError:
				messages.add_message(request, messages.ERROR, 'Upload Foto Gambar.')
				return redirect('account_settings')	

			try:
				os.remove(settings.WEBSITES['upload']['header_author']+str(kedai.header_image))
			except OSError, TypeError:
				pass	

			kedai.header_image = header_image.name
			kedai.save()
		messages.add_message(request, messages.SUCCESS, 'Mengganti Pengaturan')

		return redirect("account_settings")

	return render(request, "account/settings.html", {'list_region' : list_region, 'kedai' : kedai, 'WEBSITES' : WEBSITES, 'blogs' : blogs})

def account_detail_transaction(request, id):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		return redirect("login")
	blogs = AdminBlog.objects.order_by("-id").limit(4)		
	WEBSITES = settings.WEBSITES
	try:
		purchase = Purchase.objects.get(id=id)
	except DoesNotExist:
		return redirect('account_transaction')
	

	payment = None	
	try:	
		payment = payment_confirmation.objects.get(code_payment=purchase)
	except DoesNotExist:
		pass


	return render(request, "account/transaction/detail_transaction.html", {'kedai' : kedai, 'purchase' : purchase, 'WEBSITES' : WEBSITES, 'payment' : payment, 'blogs' : blogs})

def account_delete_transaction(request, id):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		return redirect("login")

	try:
		purchase = Purchase.objects.get(kedai=kedai, id=id)
		purchase.delete()
		messages.add_message(request, messages.SUCCESS, 'Transaksi Berhasil diHapus.')
		return redirect('account_transaction')
	except DoesNotExist:
		messages.add_message(request, messages.ERROR, 'Transaksi Tidak Ada.')
		return redirect('account_transaction')


def account_transaction(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		return redirect("login")
	blogs = AdminBlog.objects.order_by("-id").limit(4)	
	WEBSITES = settings.WEBSITES
	purchase = Purchase.objects(kedai=kedai).order_by("-id")
	return render(request, "account/transaction/transaction.html", {'kedai' : kedai, 'blogs' : blogs, 'WEBSITES' : WEBSITES, 'purchase' : purchase})	

def account_feedback(request):
	return HttpResponse("feedback")	

def admin_login(request):
	if "member_administrator" in request.session:
		return redirect("administrator")

	if request.method == "POST":
		if request.POST.get("email") and request.POST.get("password"):
			try:
				check_admin = AdminUser.objects.get(email=request.POST.get("email"))
				if check_admin.password == request.POST.get("password"):
					request.session['member_administrator'] = check_admin.email
					return redirect("administrator")
				else:
					messages.add_message(request, messages.ERROR, 'Wrong Email or Password')
			except DoesNotExist:
				messages.add_message(request, messages.ERROR, 'Wrong Email or Password')
	return render(request, "administrator/login.html", {})

def admin_edit_product(request, id):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	edit_product = Products.objects.get(id=id)		
	product_category = Product_Category.objects.order_by('category_name')
	WEBSITES = settings.WEBSITES
	return render(request, "administrator/edit/product.html", {'edit_product' : edit_product, 'product_category' : product_category, 'WEBSITES' : WEBSITES})

def admin_delete_product(request, id):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	try:
		delete_product = Products.objects.get(id=id)		
		for file_name in delete_product.images:
			os.remove(settings.WEBSITES['upload']['product']+str(delete_product.kedai.id)+"/"+file_name)
		delete_product.delete()		

		return redirect('admin_products')
	except:
		pass


def admin_products(request):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	WEBSITES = settings.WEBSITES
	products = Products.objects.order_by("-id")

	paginator = Paginator(Products.objects.order_by("-id"), 40)
	page = request.GET.get('page')
	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)

	return render(request, "administrator/products.html",{'products' : products, 'paginator' : paginator, 'WEBSITES' : WEBSITES})

def admin_delete_blog(request, id):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	try:
		blog = AdminBlog.objects.get(id=id)

		try:
			os.remove(settings.WEBSITES['upload']['blog']+str(blog.image_thumbnail))
		except OSError, TypeError:
			pass	
			
		blog.delete()
		messages.add_message(request, messages.SUCCESS, 'Blog deleted')
		return redirect("admin_blogs")

	except DoesNotExist:
		return redirect("admin_blogs")

def admin_delete_slider(request, id):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	try:
		slider = AdminSlider.objects.get(id=id)

		try:
			os.remove(settings.WEBSITES['upload']['slider']+str(slider.image))
		except OSError, TypeError:
			pass	

		slider.delete()
		return redirect("admin_slider")

	except DoesNotExist:
		return redirect("admin_slider")

def admin_delete_search(request, id):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	try:
		search = AdminSearch.objects.get(id=id)
		search.delete()

		return redirect("admin_search")
	except DoesNotExist:
		return redirect("admin_search")

def admin_delete_page(request, id):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	try:
		page =AdminPage.objects.get(id=id)
		page.delete()
		return redirect("admin_pages")
	except DoesNotExist:
		return redirect("admin_pages")


def page(request, page):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None


	WEBSITES = settings.WEBSITES
	blogs = AdminBlog.objects.order_by("-id")
	popular_products = Products.objects.order_by("-viewer").limit(5)
	try:
		page = AdminPage.objects.get(slug_url=page)
		page.page_viewer = page.page_viewer + 1
		page.save()
	except DoesNotExist:
		return redirect("index")
	return render(request, "page.html", {'page' : page, 'WEBSITES' : WEBSITES, 'blogs' : blogs, 'popular_products' : popular_products, 'kedai' : kedai})


def display_blog(request, slug):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	try:
		show_blog = AdminBlog.objects.get(slug_url=slug)
		show_blog.viewer = show_blog.viewer + 1
		show_blog.save()
	except DoesNotExist:
		return redirect("blogs")	

	WEBSITES = settings.WEBSITES	
	popular_products = Products.objects.order_by("-viewer").limit(5)
	blogs = AdminBlog.objects.order_by("-id")
	return render(request, "blog/display-blog.html", {'show_blog' : show_blog, 'WEBSITES' : WEBSITES, 'popular_products' : popular_products, 'blogs' : blogs, 'kedai' : kedai})	

def admin_edit_blog(request, id):
	WEBSITES = settings.WEBSITES
	blog = AdminBlog.objects.get(id=id)

	if request.method == "POST":
		if request.POST.get("post-name") and request.POST.get("post-content"):
			change_blog = AdminBlog.objects.get(id=id)
			change_blog.post_name = request.POST.get("post-name")
			change_blog.content = request.POST.get("post-content")
			change_blog.slug_url = slugify(change_blog.post_name)


		if request.FILES.get("post-image"):
			change_blog.image_thumbnail = random_generator()
			image_thumbnail = request.FILES.get("post-image")

			dest = open(settings.WEBSITES['upload']['blog']+change_blog.image_thumbnail, "wb+")	

			for chunk in image_thumbnail.chunks():
				dest.write(chunk)
			dest.close()

			try:
				os.remove(settings.WEBSITES['upload']['blog']+str(blog.image_thumbnail))
			except OSError, TypeError:
				pass	

		change_blog.save()

		messages.add_message(request, messages.SUCCESS, 'New Blog Added')
		return redirect('admin_edit_blog', id=blog.id)

	return render(request, "administrator/edit/blog.html", {'blog' : blog, 'WEBSITES' : WEBSITES})

def admin_create_blog(request):
	WEBSITES = settings.WEBSITES
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	if request.method == "POST":
		if request.POST.get("post-name") and request.FILES.get("post-image") and request.POST.get("post-content"):
			new_blog = AdminBlog()
			new_blog.post_name = request.POST.get("post-name")
			new_blog.content = request.POST.get("post-content")
			new_blog.slug_url = slugify(new_blog.post_name)

			if request.FILES.get("post-image"):
				file_image = request.FILES.get("post-image")
				file_image.name = random_generator()

				dest = open(settings.WEBSITES['upload']['blog']+file_image.name, "wb+")

				for chunk in file_image.chunks():
					dest.write(chunk)
				dest.close()	

				
			new_blog.image_thumbnail = file_image.name
			new_blog.save()	

			messages.add_message(request, messages.SUCCESS, 'New Blog Added')
			return redirect('admin_edit_blog', id=new_blog.id)
				

	return render(request, "administrator/create/blog.html",{'WEBSITES' : WEBSITES})

def admin_create_slider(request):
	WEBSITES = settings.WEBSITES
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	if request.method == "POST":
		if request.POST.get("slider-name") and request.FILES.get("post-image"):
			new_slider = AdminSlider()
			new_slider.slider_name = request.POST.get("slider-name")
			new_slider.content = request.POST.get("slider-content")
			new_slider.redirect_url = request.POST.get("redirect_url")

			if request.FILES.get("post-image"):
				file_image = request.FILES.get("post-image")
				file_image.name = random_generator()

				dest = open(settings.WEBSITES['upload']['slider']+file_image.name, "wb+")

				for chunk in file_image.chunks():
					dest.write(chunk)
				dest.close()	

			new_slider.image = file_image.name	
			new_slider.save()

			messages.add_message(request, messages.SUCCESS, 'New Slider Added')
			return redirect("admin_edit_slider", id=new_slider.id)						
	return render(request, "administrator/create/slider.html", {'WEBSITES' : WEBSITES})

def admin_create_page(request):
	WEBSITES = settings.WEBSITES
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	if request.method == "POST":
		if request.POST.get("page-name") and request.POST.get("page-content") and request.POST.get("page-slug-url"):
			new_page = AdminPage() 
			new_page.page_name = request.POST.get("page-name")
			new_page.content = request.POST.get("page-content")
			new_page.slug_url = request.POST.get("page-slug-url")
			new_page.save()

			messages.add_message(request, messages.SUCCESS, 'New Page Added')
			return redirect("admin_edit_page", id=new_page.id)			
	return render(request, "administrator/create/page.html", {'WEBSITES' : WEBSITES})

def admin_create_category(request):
	WEBSITES = settings.WEBSITES
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	if request.method == "POST":
		if request.POST.get("category-name") and request.POST.get("icon-css-name") and request.POST.get("slug-url-name"):
			new_category = Product_Category()
			new_category.category_name = request.POST.get("category-name")
			new_category.css_class_name = request.POST.get("icon-css-name")
			new_category.slug_url = request.POST.get("slug-url-name")
			new_category.save()

			messages.add_message(request, messages.SUCCESS, 'New Category Added')
			return redirect("admin_edit_category", id=new_category.id)

	return render(request, "administrator/create/category.html", {'WEBSITES' : WEBSITES})

def admin_blogs(request):
	WEBSITES = settings.WEBSITES
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None
	blogs = AdminBlog.objects.order_by("-id")

	return render(request, "administrator/blogs.html", {'blogs' : blogs, 'kedai' : kedai, 'WEBSITES' : WEBSITES})

def administrator(request):
	if "member_administrator" not in request.session:
		return redirect("admin_login")

	WEBSITES = settings.WEBSITES
	all_kedai = Kedai.objects
	all_products = Products.objects	
	all_categories = Product_Category.objects
	return render(request, 'administrator/administrator.html', { 'WEBSITES' : WEBSITES, 'all_kedai' : all_kedai, 'all_products' : all_products, 'all_categories' : all_categories} )


def admin_edit_region(request, id=id):
	WEBSITES = settings.WEBSITES
	region = ListRegion.objects.get(id=id)
	if request.method == "POST":
		if request.POST.get("region-name") and request.POST.get("latitude") and request.POST.get("longtitude"):
			region.region = request.POST.get("region-name")
			region.latitude = request.POST.get("latitude")
			region.longtitude = request.POST.get("longtitude")
			region.save()

			messages.add_message(request, messages.SUCCESS, 'Region Changed')
	return render(request, "administrator/edit/region.html", {'region' : region, 'WEBSITES' : WEBSITES})

def admin_edit_slider(request, id):
	WEBSITES = settings.WEBSITES
	slider = AdminSlider.objects.get(id=id)
	if request.method == "POST":
		if request.POST.get("slider-name") and request.POST.get("slider-content"):
			slider.slider_name = request.POST.get("slider-name")
			slider.content = request.POST.get("slider-content")
			slider.redirect_url = request.POST.get("redirect_url")

			if request.FILES.get("post-image"):
				file_image = request.FILES.get("post-image")
				file_image.name = random_generator()

				dest = open(settings.WEBSITES['upload']['slider']+file_image.name, "wb+")

				for chunk in file_image.chunks():
					dest.write(chunk)
				dest.close()	

				try:
					os.remove(settings.WEBSITES['upload']['slider']+str(slider.image))
				except OSError, TypeError:
					pass	

			slider.image = file_image.name	
			slider.save()
			messages.add_message(request, messages.SUCCESS, 'Slider Changed')
	return render(request, "administrator/edit/slider.html", {'slider' : slider, 'WEBSITES' : WEBSITES})

def admin_slider(request):
	WEBSITES = settings.WEBSITES
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	sliders = AdminSlider.objects
	return render(request, "administrator/sliders.html", {'sliders' : sliders, 'WEBSITES' : WEBSITES})


def admin_security_question(request):

	return render(request, "administrator/security_question.html", {})



def admin_search(request):
	search = AdminSearch.objects.order_by("-id")

	return render(request, "administrator/search.html", {'search' : search})

def admin_regions(request):
	WEBSITES = settings.WEBSITES
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	list_region = ListRegion.objects.order_by("-id")
	return render(request, "administrator/regions.html", {'list_region' : list_region, 'WEBSITES' : WEBSITES})


def admin_shipping_cost(request):
	WEBSITES = settings.WEBSITES
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	list_province = ListProvince.objects.order_by("province")
	return render(request, "administrator/shipping_costs.html", {'WEBSITES' : WEBSITES, 'list_province' : list_province})	

def admin_edit_shipping(request, id):
	WEBSITES = settings.WEBSITES
	try:
		get_province = ListProvince.objects.get(id=id)
	except DoesNotExist:
		return redirect("admin_shipping_cost")

	if request.method == "POST":
		if request.POST.get("province") and request.POST.get("shipping_cost"):
			get_province.province = request.POST.get("province")
			get_province.shipping_cost = request.POST.get("shipping_cost")
			get_province.save()

			messages.add_message(request, messages.SUCCESS, 'Province Shipping Cost Changed')	

	return render(request, "administrator/edit/shipping_cost.html", {'get_province' : get_province, 'WEBSITES' : WEBSITES})

def admin_edit_category(request, id):
	WEBSITES = settings.WEBSITES
	category = Product_Category.objects.get(id=id)
	if request.method == "POST":
		if request.POST.get("category_name") and request.POST.get("css_name") and request.POST.get("slug_url"):
			category.category_name = request.POST.get("category_name")
			category.css_class_name = request.POST.get("css_name")
			category.slug_url = request.POST.get("slug_url")

			category.save()
			messages.add_message(request, messages.SUCCESS, 'Category Changed')

	return render(request, "administrator/edit/category.html", {'category' : category, 'WEBSITES' : WEBSITES})


def admin_edit_page(request, id):
	WEBSITES = settings.WEBSITES
	page = AdminPage.objects.get(id=id)
	if request.method == "POST":
		if request.POST.get("page_name") and request.POST.get("content") and request.POST.get("slug_url"):
			page.page_name = request.POST.get("page_name")
			page.content = request.POST.get("content")
			page.slug_url = request.POST.get("slug_url")

			page.save()
			messages.add_message(request, messages.SUCCESS, 'Page Changed')

	return render(request, "administrator/edit/page.html", {'page' : page, 'WEBSITES' : WEBSITES})

def admin_pages(request):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	WEBSITES = settings.WEBSITES
	pages = AdminPage.objects.order_by("-id")
	return render(request, "administrator/pages.html", {'pages' : pages, 'WEBSITES' : WEBSITES})


def admin_categories(request):
	if "member_administrator" not in request.session:
		return redirect("admin_login")
	WEBSITES = settings.WEBSITES
	all_categories = Product_Category.objects
	return render(request, 'administrator/categories.html', {'all_categories' : all_categories, 'WEBSITES' : WEBSITES})

def admin_users(request):
	if "member_administrator" not in request.session:
		return redirect("admin_login")

	WEBSITES = settings.WEBSITES	
	paginator = Paginator(Kedai.objects.order_by("-date_add"), 40)
	page = request.GET.get('page')
	try:
		all_kedai = paginator.page(page)
	except PageNotAnInteger:
		all_kedai = paginator.page(1)
	except EmptyPage:
		all_kedai = paginator.page(paginator.num_pages)

	return render(request, 'administrator/users.html', {'all_kedai' : all_kedai, 'paginator' : paginator, 'WEBSITES' : WEBSITES} )	



def admin_edit_user(request, id):

	list_region = ListRegion.objects
	kedai = Kedai.objects.get(id=id)
	WEBSITES = settings.WEBSITES	

	if request.method == "POST":
		if request.POST.get("store_name") and request.POST.get("email") and request.POST.get("phone_number") and request.POST.get("address") and request.POST.get("owner_name") and \
		request.POST.get("postal_code") and request.POST.get("region") and request.POST.get("about"):
			kedai.store_name = request.POST.get("store_name")
			kedai.address = request.POST.get("address")
			kedai.region =  ListRegion.objects.get(id=request.POST.get("region"))
			kedai.postal_code = request.POST.get("postal_code")
			kedai.phone_number = request.POST.get("phone_number")
			kedai.store_url_slug = slugify(kedai.store_name)
			kedai.about_store = request.POST.get("about")
			kedai.save()

		if request.FILES.get("logo_img"):
			logo_img = request.FILES.get("logo_img")
			logo_img.name = random_generator()

			dest = open(settings.WEBSITES['upload']['photo_profile']+logo_img.name, "wb+")

			for chunk in logo_img.chunks():
				dest.write(chunk)
			dest.close()
			
			try:
				os.remove(settings.WEBSITES['upload']['photo_profile']+str(kedai.logo_image))
			except OSError, TypeError:
				pass	

			kedai.logo_image = logo_img.name
			kedai.save()

		user = Users.objects.get(id=kedai.author.id)
		user.email = request.POST.get("email")
		user.full_name = request.POST.get("owner_name")

		if request.POST.get("is_active") == "active" :
			user.is_active = True
		else:
			user.is_active = False

		if request.POST.get("is_validated") == "confirmed":
			user.is_validated = True
		else:
			user.is_validated = False
		
		
		user.save()

		messages.add_message(request, messages.SUCCESS, 'Mengganti profil')
		return redirect("admin_edit_user", id=id)

	return render(request, 'administrator/edit/user.html', {'kedai' : kedai, 'list_region' : list_region, 'WEBSITES' : WEBSITES})

def blogs(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	WEBSITES = settings.WEBSITES	
	popular_products = Products.objects.order_by("-viewer").limit(5)
	side_blogs = AdminBlog.objects.order_by("-id")

	page = request.GET.get('page')
	paginator = Paginator(AdminBlog.objects.order_by('-id'), 7)
	try:
		blogs = paginator.page(page)
	except PageNotAnInteger:
		blogs = paginator.page(1)
	except EmptyPage:
		blogs = paginator.page(paginator.num_pages)	

	return render(request, 'blog/blogs.html', {'kedai' : kedai, 'WEBSITES' : WEBSITES, 'popular_products' : popular_products, 'blogs' : blogs, 'paginator' : paginator, 'side_blogs' : side_blogs})

def beranda(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	page = request.GET.get('page')
	# if request.GET.get('sort', "popularity"):
	# 	paginator = Paginator(Products.objects.order_by("-viewer"), 40)		
	# if request.GET.get('sort', "cheap"):
	# 	paginator = Paginator(Products.objects.order_by("price"), 40)		
	# if request.GET.get('sort', "expensive"):
	# 	paginator = Paginator(Products.objects.order_by("-price"), 40)			
	# if request.GET.get('sort', "random"):
	# 	paginator = Paginator([item for item in Products.objects.order_by("-price")], 40)				
	# 	# random.shuffle(paginator)    		
	# else:
	paginator = Paginator(Products.objects.order_by("-id"), 15)

	WEBSITES = settings.WEBSITES
	product_category = Product_Category.objects.order_by('category_name')
	popular_products = Products.objects.order_by("-viewer").limit(5)
	blogs = AdminBlog.objects.order_by("-id").limit(4)

	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)

	return render(request, 'index/beranda.html', {'WEBSITES' : WEBSITES, 'product_category' : product_category, 'products' : products, 'paginator': paginator, 'kedai' : kedai, 'popular_products' : popular_products, 'blogs' : blogs})

def index_thumbnails(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	WEBSITES = settings.WEBSITES
	popular_products = Products.objects.order_by("-viewer").limit(5)

	product_category = Product_Category.objects.order_by('category_name')

	blogs = AdminBlog.objects.order_by("-id").limit(4)

	paginator = Paginator(Products.objects.order_by("-id"), 7)
	page = request.GET.get('page')
	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)
	return render(request, 'index/index-thumbnails.html', {'WEBSITES' : WEBSITES, 'product_category' : product_category, 'products' : products, 'paginator' : paginator, 'kedai' : kedai, 'popular_products' : popular_products, 'blogs' : blogs})	

def category(request, slug_url):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	WEBSITES = settings.WEBSITES

	popular_products = Products.objects.order_by("-viewer").limit(5)
	product_category = Product_Category.objects.order_by('category_name')
	category_choiced = Product_Category.objects.get(slug_url=slug_url)
	blogs = AdminBlog.objects.order_by("-id").limit(4)

	paginator = Paginator(Products.objects(category=category_choiced).order_by("-id"), 30)

	page = request.GET.get('page')

	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)
	return render(request, 'category/category.html', {'WEBSITES' : WEBSITES, 'product_category' : product_category, 'products' : products, 'category_choiced' : category_choiced, 'paginator' : paginator, 'kedai' : kedai, 'popular_products' : popular_products, 'blogs' : blogs})		

def category_thumbnails(request, slug_url): 
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	WEBSITES = settings.WEBSITES
	popular_products = Products.objects.order_by("-viewer").limit(5)
	product_category = Product_Category.objects.order_by('category_name')
	category_choiced = Product_Category.objects.get(slug_url=slug_url)
	paginator = Paginator(Products.objects(category=category_choiced).order_by("-id"), 10)
	page = request.GET.get('page')

	blogs = AdminBlog.objects.order_by("-id").limit(4)

	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)
	return render(request, 'category/category-thumbnails.html', {'WEBSITES' : WEBSITES, 'product_category' : product_category, 'products' : products, 'category_choiced' : category_choiced, 'paginator' : paginator, 'kedai' : kedai, 'popular_products' : popular_products, 'blogs' : blogs})		

def login(request):
	if "member" in request.session:
		return redirect("profile")
	WEBSITES = settings.WEBSITES
	blogs = AdminBlog.objects.order_by("-id").limit(4)
	if request.method == "POST":
		if request.POST.get("email") and request.POST.get("pass"):
			try:
				check_user = Users.objects.get(email=request.POST.get("email"))
				#account suspened
				if check_user.is_active == False:
					messages.add_message(request, messages.ERROR, 'Akun Anda telah disuspend.')
					return redirect("login") 

                		if check_user.password == request.POST.get("pass"):
                			#make last login
                			check_user.last_login = datetime.datetime.now()
                			check_user.save()

                    			request.session['member'] = check_user.email
                			return redirect("profile")
                		else:
                			messages.add_message(request, messages.ERROR, 'Password salah')
                	
			except Users.DoesNotExist:
				messages.add_message(request, messages.ERROR, 'Anda belum terdaftar')
	return render(request, 'login.html', {'WEBSITES' : WEBSITES, 'blogs' : blogs})

def register(request):
	if "member" in request.session:
		return redirect("profile")
	WEBSITES = settings.WEBSITES
	blogs = AdminBlog.objects.order_by("-id").limit(4)
	if request.method == "POST":
		if request.POST.get("seller_name") and request.POST.get("store") and request.POST.get("email") and request.POST.get("pass") and request.POST.get("pass0"):
			if request.POST.get("pass") != request.POST.get("pass0"):
				messages.add_message(request, messages.ERROR, 'Password tidak sama')
				return redirect("register")
				
			try:
				register = Users()
				register.email = request.POST.get("email")
				register.full_name = request.POST.get("seller_name")
				register.password = request.POST.get("pass")
				register.save()
			
				kedai = Kedai()
				kedai.author = register
				kedai.store_name = request.POST.get("store")
				kedai.store_url_slug = slugify(kedai.store_name)
				kedai.map_latitude = "5.549013715023947"
				kedai.map_longtitude = "95.31978607177734"
				kedai.save()

				os.makedirs(settings.WEBSITES['upload']['product'] + str(kedai.id))
				os.makedirs(settings.WEBSITES['upload']['product_thumbnails'] + str(kedai.id))

				social_media = SocialMedia(kedai=kedai).save()

				# send email for verification
				link = "http://keudee.com"+reverse('email_verification', kwargs={'id':register.id})
				send_email_verification(from_email=settings.EMAIL_HOST_USER, to=register.email, full_name=register.full_name, link=link)

				messages.add_message(request, messages.SUCCESS, 'Berhasil terdaftar, mohon buka email Anda untuk konfirmasi.')

			except NotUniqueError:
				messages.add_message(request, messages.ERROR, 'Nama Kedai atau Email sudah terdaftar')
					
	return render(request, 'register.html', {'WEBSITES' : WEBSITES, 'blogs' : blogs})

def show_product(request):
	WEBSITES = settings.WEBSITES
	blogs = AdminBlog.objects.order_by("-id").limit(4)
	return render(request, 'show_product.html', {'WEBSITES' : WEBSITES})

def change_password(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		return redirect("login")

	blogs = AdminBlog.objects.order_by("-id").limit(4)	
	user = Users.objects.get(email=request.session.get('member'))
	WEBSITES = settings.WEBSITES
	if request.method == "POST":
		if request.POST.get("last_password") and request.POST.get("new_password1") and request.POST.get("new_password2"):
			if request.POST.get("new_password1") != request.POST.get("new_password2"):
				messages.add_message(request, messages.ERROR, 'Password tidak sama')
			if request.POST.get("last_password") != user.password:
				messages.add_message(request, messages.ERROR, 'Password lama salah')
			if request.POST.get("last_password") == user.password and request.POST.get("new_password1") == request.POST.get("new_password2"):
				user.password = request.POST.get("new_password1")
				user.save()
				messages.add_message(request, messages.SUCCESS, 'Password terganti')
		else:
			messages.add_message(request, messages.ERROR, 'Isikan semua form')				
	return render(request, 'account/change-password.html', {'WEBSITES' : WEBSITES, 'kedai' : kedai, 'blogs' : blogs})	

def log_out(request):
	try:
		del request.session["member"]
	except KeyError:
		pass
	return redirect("index")

def profile(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		return redirect("login")

	# make last login/activity
	user = Users.objects.get(email=request.session.get('member'))
	user.last_login = datetime.datetime.now()
	user.save()

	blogs = AdminBlog.objects.order_by("-id").limit(4)	
	kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')))
	list_region = ListRegion.objects
	WEBSITES = settings.WEBSITES
	if request.method == "POST":
		if request.POST.get("store_name") and request.POST.get("email") and request.POST.get("phone_number") and request.POST.get("address") and request.POST.get("owner_name") and \
		request.POST.get("postal_code") and request.POST.get("region") and request.POST.get("about"):
			try:
				kedai.store_name = re.sub('\W+','', request.POST.get("store_name"))
				if kedai.store_name == "" or len(kedai.store_name) < 5 :
					messages.add_message(request, messages.ERROR, 'Mohon isi nama kedai, tanpa karakter spesial, dan minimal 5 karakter')
					return redirect("profile")				
				kedai.address = request.POST.get("address")
				kedai.region =  ListRegion.objects.get(id=request.POST.get("region"))
				kedai.postal_code = request.POST.get("postal_code")
				kedai.phone_number = request.POST.get("phone_number")
				kedai.store_url_slug = slugify(kedai.store_name)
				kedai.about_store = request.POST.get("about")
				kedai.save()
			except NotUniqueError:
				messages.add_message(request, messages.ERROR, 'Nama kedai sudah dipakai oleh orang lain.')
				return redirect("profile")
			try:
				user = Users.objects.get(email=request.session.get('member'))
				current_email = user.email
				user.email = request.POST.get("email")
				user.full_name = request.POST.get("owner_name")


				#automatic change session if email changed
				del request.session["member"]
				request.session['member'] = user.email

				user.save()
			except NotUniqueError:
				del request.session["member"]
				request.session['member'] = current_email
				messages.add_message(request, messages.ERROR, 'Email sudah digunakan oleh orang lain.')				
				return redirect("profile")

		if request.FILES.get("logo_img"):
			logo_img = request.FILES.get("logo_img")
			logo_img.name = random_generator() + ".jpg"

			dest = open(settings.WEBSITES['upload']['photo_profile']+logo_img.name, "wb+")

			for chunk in logo_img.chunks():
				dest.write(chunk)
			dest.close()

			try:
				compress_change_ext(settings.WEBSITES['upload']['photo_profile']+logo_img.name, target=settings.WEBSITES['upload']['photo_profile']+logo_img.name, quality=15)
			except IOError:
				messages.add_message(request, messages.ERROR, 'Mohon Diupload Gambar.')	
				return redirect('profile')

			try:
				os.remove(settings.WEBSITES['upload']['photo_profile']+str(kedai.logo_image))
			except OSError, TypeError:
				pass	

			kedai.logo_image = logo_img.name
			kedai.save()

		messages.add_message(request, messages.SUCCESS, 'Mengganti profil')
		return redirect("profile")

	return render(request, 'account/profile.html', {'WEBSITES' : WEBSITES, 'kedai' : kedai, 'list_region' : list_region, 'blogs' : blogs })	


def social_media(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		return redirect("login")


	WEBSITES = settings.WEBSITES
	blogs = AdminBlog.objects.order_by("-id").limit(4)	
	kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')))
	social_media = SocialMedia.objects.get(kedai=kedai)
	if request.method == "POST":
		social_media.facebook = request.POST.get("facebook")
		social_media.instagram = request.POST.get("instagram")
		social_media.twitter = request.POST.get("twitter")
		social_media.google_plus = request.POST.get("google_plus")
		social_media.pinterest = request.POST.get("pinterest")
		social_media.BBM = request.POST.get("bbm")
		social_media.save()

		messages.add_message(request, messages.SUCCESS, 'Mengubah Akun Social Media')
	return render(request, 'account/social_media.html', {'WEBSITES' : WEBSITES, 'social_media' : social_media, 'kedai' : kedai, 'blogs' : blogs})		


def handle_upload_file(f,location):
	with open(location+f.name, "wb+") as destination:
		for chunk in f.chunks():
			destination.write(chunk)
		destination.close()		


def add_product(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		return redirect("login")

	check_validate = Users.objects.get(email=request.session.get('member'))	
	if check_validate.is_validated == False:
		messages.add_message(request, messages.ERROR, 'Anda tidak bisa menambah produk karena belum melakukan validasi melalui email, Silahkan buka email.')	
		return redirect("profile")

	blogs = AdminBlog.objects.order_by("-id").limit(4)		
	WEBSITES = settings.WEBSITES
	kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')))
	product_category = Product_Category.objects.order_by('category_name')

	if request.method == "POST":
		if request.POST.get('name') and request.POST.get('category') and request.POST.get('price') and request.POST.get("quantity") and request.POST.get("category") and request.POST.get("information") and request.FILES.getlist('photo'):
			price = request.POST.get('price')
			price = re.findall('\d+', price)
			price = "".join(price)

			new_product = Products(kedai=kedai)
			new_product.product_name = request.POST.get('name')
			new_product.price = price 
			new_product.quantity = request.POST.get('quantity')
			new_product.shipping_and_payment = request.POST.get('shipping_payment')
			new_product.weight = request.POST.get('weight')
			new_product.slug_url = slugify(new_product.product_name + " " + random_generator())
			# new_product.tags = request.POST.get("tags").split()

			if request.POST.get("condition") == "new":
				new_product.condition = True
			else:
				new_product.condition = False	

			new_product.information = request.POST.get("information")
			new_product.category = Product_Category.objects.get(id=request.POST.get('category'))

			if request.POST.get("sub_category"):
				new_product.sub_category = Sub_Product_Category.objects.get(id=request.POST.get('sub_category'))				

			# Upload Images
			for upfile in request.FILES.getlist('photo'):
				upfile.name = random_generator() + ".jpg"
				handle_upload_file(f=upfile, location=settings.WEBSITES['upload']['product']+str(kedai.id)+"/")	
				new_product.images = [upfile.name] + new_product.images

			# Compress and Change Extension to .jpg
			for img_name in new_product.images:
				compress_change_ext(src=settings.WEBSITES['upload']['product']+str(kedai.id)+"/"+img_name, target=settings.WEBSITES['upload']['product']+str(kedai.id)+"/"+img_name)

			# Make thumbnail first image
			img_src = settings.WEBSITES['upload']['product']+str(kedai.id)+"/"+new_product.images[0]
			img_save_target = settings.WEBSITES['upload']['product_thumbnails']+str(kedai.id)+"/"+new_product.images[0]
			img_crop_square(src=img_src, target=img_save_target, w=300, h=300)

			new_product.save()

			messages.add_message(request, messages.SUCCESS, 'Menambahkan produk baru')	
			return redirect('edit_product', id=new_product.id)

		else:
			messages.add_message(request, messages.ERROR, 'Isi semua form.')	

	return render(request, 'account/add-product.html', {'WEBSITES' : WEBSITES, 'product_category' : product_category, 'kedai' : kedai, 'blogs' : blogs})			

def edit_product(request, id):
	blogs = AdminBlog.objects.order_by("-id").limit(4)		
	kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')))
	edit_product = Products.objects.get(id=id)
	WEBSITES = settings.WEBSITES
	product_category = Product_Category.objects.order_by('category_name')
	if request.method == "POST":
		if request.POST.get('name') and request.POST.get('category') and request.POST.get('price') and request.POST.get("quantity") and request.POST.get("category") and request.POST.get("information"):
			price = request.POST.get('price')
			price = re.findall('\d+', price)
			price = "".join(price)

			edit_product.product_name = request.POST.get('name')
			edit_product.price = price 
			edit_product.quantity = request.POST.get('quantity')
			edit_product.weight = request.POST.get('weight')
			edit_product.shipping_and_payment = request.POST.get('shipping_payment')
			edit_product.last_update = datetime.datetime.now()
			edit_product.slug_url = slugify(edit_product.product_name + " " + random_generator())
			# edit_product.tags = request.POST.get("tags").split()

			if request.POST.get("condition") == "new":
				edit_product.condition = True
			else:
				edit_product.condition = False	

			edit_product.information = request.POST.get("information")
			edit_product.category = Product_Category.objects.get(id=request.POST.get('category'))

			if request.POST.get("sub_category"):
				edit_product.sub_category = Sub_Product_Category.objects.get(id=request.POST.get('sub_category'))				

			if request.FILES.getlist('photo'):
				try:
					for file_name in edit_product.images:
						os.remove(settings.WEBSITES['upload']['product']+str(kedai.id)+"/"+file_name)
				except:
					pass

				edit_product.images = []

				for upfile in request.FILES.getlist('photo'):
					upfile.name = random_generator() + ".jpg"
					handle_upload_file(f=upfile, location=settings.WEBSITES['upload']['product']+str(kedai.id)+"/")	
					edit_product.images = [upfile.name] + edit_product.images

				# Compress and Change Extension to .jpg
				for img_name in edit_product.images:
					compress_change_ext(src=settings.WEBSITES['upload']['product']+str(kedai.id)+"/"+img_name, target=settings.WEBSITES['upload']['product']+str(kedai.id)+"/"+img_name)	
			
				# Make thumbnail first image
				img_src = settings.WEBSITES['upload']['product']+str(kedai.id)+"/"+edit_product.images[0]
				img_save_target = settings.WEBSITES['upload']['product_thumbnails']+str(kedai.id)+"/"+edit_product.images[0]
				img_crop_square(src=img_src, target=img_save_target, w=300, h=300)	

			edit_product.save()

			messages.add_message(request, messages.SUCCESS, 'Mengganti Produk')	
			return redirect('edit_product', id=edit_product.id)

		else:
			messages.add_message(request, messages.ERROR, 'Isi semua form')	
	return render(request, 'account/edit-product.html', {'WEBSITES' : WEBSITES, 'kedai' : kedai, 'edit_product' : edit_product, 'product_category' : product_category, 'blogs' : blogs})	

def delete_product(request, id):
	kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')))
	if not "member" in request.session:
		return redirect("index")
	try:
		delete_product = Products.objects.get(id=id)		
		for file_name in delete_product.images:
			os.remove(settings.WEBSITES['upload']['product']+str(kedai.id)+"/"+file_name)

		#delete thumbnail
		os.remove(settings.WEBSITES['upload']['product_thumbnails']+str(kedai.id)+"/"+delete_product.images[0])
		
		delete_product.delete()		

		messages.add_message(request, messages.SUCCESS, 'Produk terhapus')	
		return redirect('products')
	except:
		delete_product.delete()		

	return redirect('products')	

def search(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	paginator = Paginator(Products.objects(status_publish=True).order_by("-id"), 12)	

	get_search = ""	
	if request.GET.get("q"):
		get_search = request.GET.get("q")
		paginator = Paginator(Products.objects(product_name__icontains=get_search).order_by("-id"), 12)


	page = request.GET.get('page')

	# SEARCH SORT
	# variable value in search.html
	# 1 = New Product
	# 2 = Second Product
	# 3 = Highest Price
	# 4 = Lowest Price
	# 5 = Most Viewed
	sort_product=""
	if request.GET.get("sort_product") == "1":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(product_name__icontains=get_search).order_by("-id"), 12)		


	popular_products = Products.objects.order_by("-viewer").limit(5)
	blogs = AdminBlog.objects.order_by("-id").limit(4)		
	new_search = AdminSearch(search=get_search)
	new_search.save()

	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)	

	WEBSITES = settings.WEBSITES	
	product_category = Product_Category.objects.order_by('category_name')

	return render(request, 'search.html', {'WEBSITES' : WEBSITES, 'products' : products, 'product_category' : product_category, 'paginator' : paginator, 'get_search' : get_search, 'kedai' : kedai, 'blogs' : blogs, 'popular_products' : popular_products, "sort_product" : sort_product})				


def products(request):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		return redirect("login")

	# make last login/activity
	user = Users.objects.get(email=request.session.get('member'))
	user.last_login = datetime.datetime.now()
	user.save()
		
	WEBSITES = settings.WEBSITES
	blogs = AdminBlog.objects.order_by("-id").limit(4)	
	kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')))
	products = Products.objects(kedai=kedai).order_by("-id")

	paginator = Paginator(Products.objects(kedai=kedai).order_by("-id"), 7)
	page = request.GET.get('page')

	
	# sort_product for sorting product
	# 1 == newest product
	# 2 == product name (sort by product name like alphabet)
	# 3 == most viewed
	# 4 == lowest price
	# 5 == highest price
	sort_product = ""
	if request.GET.get("sort_product") == "1":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=kedai).order_by("-id"), 8)

	if request.GET.get("sort_product") == "2":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=kedai).order_by("product_name"), 8)	

	if request.GET.get("sort_product") == "3":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=kedai).order_by("-viewer"), 8)			

	if request.GET.get("sort_product") == "4":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=kedai).order_by("price"), 8)				

	if request.GET.get("sort_product") == "5":
		sort_product = request.GET.get("sort_product")
		paginator = Paginator(Products.objects(kedai=kedai).order_by("-price"), 8)						

	# sort for condition, 0 is for both of new and second. 1 is new and 2 is second product.
	condition = ""
	if request.GET.get("condition") == "0":
		condition = request.GET.get("condition")
		paginator = Paginator(Products.objects(kedai=kedai).order_by("-id"), 8)

	if request.GET.get("condition") == "1":
		condition = request.GET.get("condition")
		paginator = Paginator(Products.objects(kedai=kedai, condition=True).order_by("-id"), 8)

	if request.GET.get("condition") == "2":
		condition = request.GET.get("condition")
		paginator = Paginator(Products.objects(kedai=kedai, condition=False).order_by("-id"), 8)	

	search_product = ""
	if request.GET.get("q"):
		search_product = request.GET.get("q")
		paginator = Paginator(Products.objects(kedai=kedai, product_name__icontains=request.GET.get('q')).order_by("-id"), 7)

	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(1)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)

	return render(request, 'account/products.html', {'WEBSITES' : WEBSITES, 'products' : products, 'paginator' : paginator, 'kedai' : kedai, 'blogs' : blogs, 'search_product' : search_product, 'condition' : condition, 'sort_product' : sort_product})			

def display_product_by_id(request, id):
	try:
		product = Products.objects.get(id=id)
		return redirect("display_product", slug=product.slug_url)
	except DoesNotExist:
		return redirect("index")

def display_product(request, slug):
	if "member" in request.session:
		kedai = Kedai.objects.get(author=Users.objects.get(email=request.session.get('member')) )
	else:
		kedai = None

	WEBSITES = settings.WEBSITES
	try:
		product = Products.objects.get(slug_url=slug)
		product.viewer = product.viewer + 1
		product.save()
	except DoesNotExist:
		return redirect("index")

	#the owner of product cannot buy their self product	
	owner_product = False	
	if product.kedai == kedai:
		owner_product = True	

	blogs = AdminBlog.objects.order_by("-id").limit(4)

	popular_products = Products.objects.order_by("-viewer").limit(5)
	recommend_products = Products.objects(category=product.category).order_by("-id")

	return render(request, 'display-product.html', {'WEBSITES' : WEBSITES, 'product' : product, 'kedai' : kedai, 'recommend_products' : recommend_products, 'popular_products' : popular_products, 'blogs' : blogs, 'owner_product' : owner_product})				

def ajax_sub_category(request):
	if request.method == "POST":
		category = request.POST.get("category")
		sub_category = Sub_Product_Category.objects(category_index=Product_Category.objects.get(id=category)).order_by('category_name')
		data = []
		for sub_category in sub_category:
			data.append("<option value='%s'>%s</option>" % (sub_category.id, sub_category.category_name ))
		return HttpResponse(data)