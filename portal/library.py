import random, string
from PIL import Image
from models import *
from slugify import slugify


class RegisterKedai(object):
	pass


def random_generator():
	rand = [random.choice(string.letters+string.digits) for x in xrange(10)]
	rand = "".join(rand)
	return rand

def compress_change_ext(src, target, quality=80):
    img = Image.open(src)
    img.save(target, quality=quality, optimized=True)

def img_crop_square(src, target, w=200, h=200):
	"""
		src is img want to opened
		target is where img want to saved 
		w and h is size width and height 
	"""
	img = Image.open(src)

	size = min(img.size)
	originX = img.size[0] / 2 - size / 2
	originY = img.size[1] / 2 - size / 2
	cropBox = (originX, originY, originX + size, originY + size)

	save_img = img.crop(cropBox)
	save_img = save_img.resize((w, h), Image.ANTIALIAS)
	save_img.save(target, optimize=True)



class Register():
	def __init__(self, kedai_name, email, password):
		register = Users()
		register.email = email
		register.full_name = kedai_name
		register.password = password
		register.is_validated = False
		register.save()

		kedai = Kedai()
		kedai.author = register
		kedai.store_name = kedai_name
		kedai.store_url_slug = slugify(u'%s') % (kedai.store_name)
		kedai.save()

		os.makedirs(settings.WEBSITES['upload']['product'] + str(kedai.id))
		os.makedirs(settings.WEBSITES['upload']['product_thumbnails'] + str(kedai.id))

		social_media = SocialMedia(kedai=kedai).save()
