import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir("/home/fird0s/sites/project/keudee/local/lib/python2.7/site-packages")

# Add the app's directory to the PYTHONPATH
sys.path.append("/home/fird0s/sites/project/keudee/kedaia")
sys.path.append("/home/fird0s/sites/project/keudee/kedaia/portal")

os.environ['DJANGO_SETTINGS_MODULE'] = 'kedaia.settings'

# Activate your virtual env
activate_env=os.path.expanduser("/home/fird0s/sites/project/keudee/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

